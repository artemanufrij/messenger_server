var mongo = require("mongodb");

var url = "mongodb://localhost:27017/";
var database = "messenger";
var db;

mongo.connect(url, { useNewUrlParser: true }, (err, client) => {
  if (err) {
    throw err;
  }
  db = client.db(database);

  exports.roles = callback => {
    db.collection("roles")
      .find({})
      .toArray((err, result) => {
        callback(result);
      });
  };
  exports.tasks = callback => {
    db.collection("tasks")
      .find({ done: undefined, cancel: undefined })
      .toArray((err, result) => {
        callback(result);
      });
  };

  exports.tasks_for = (receiver, callback) => {
    db.collection("tasks")
      .find({
        "command.receiver": receiver,
        done: undefined,
        cancel: undefined
      })
      .toArray((err, result) => {
        callback(
          result.sort((a, b) => {
            if (a.command.sort != b.command.sort) {
              return a.command.sort - b.command.sort;
            }
            return a.timestamp - b.timestamp;
          })
        );
      });
  };

  exports.tasks_from = (sender, callback) => {
    db.collection("tasks")
      .find({ sender: sender, done: undefined, cancel: undefined })
      .toArray((err, result) => {
        callback(
          result.sort((a, b) => {
            if (a.command.sort != b.command.sort) {
              return a.command.sort - b.command.sort;
            }
            return a.timestamp - b.timestamp;
          })
        );
      });
  };

  exports.new_task = (task, callback) => {
    db.collection("tasks").insertOne(task, (err, result) => {
      callback();
    });
  };
  exports.close_task = (task, callback) => {
    db.collection("tasks").updateOne(
      { _id: new mongo.ObjectId(task) },
      { $set: { done: Date.now() } },
      (err, result) => {
        callback(result);
      }
    );
  };
  exports.cancel_task = (task, callback) => {
    db.collection("tasks").updateOne(
      { _id: new mongo.ObjectId(task) },
      { $set: { cancel: Date.now() } },
      (err, result) => {
        callback(result);
      }
    );
  };
  exports.update_task = (task, callback) => {
    db.collection("tasks").updateOne(
      { _id: task._id },
      { $set: { timestamp: task.timestamp } },
      (err, result) => {
        callback();
      }
    );
  };
});
