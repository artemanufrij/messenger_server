var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);

var database = require("./interfaces/database");

var port = 3010;

io.origins(["*:*"]);

connections = [];
stations = [
  {
    title: "Wareneingang",
    name: "wareneingang",
    commands: [
      {
        title: "Paletten abholen",
        receiver: "Rennameise",
        priority: "height",
        sort: "0"
      },
      {
        title: "Kisten abholen",
        receiver: "Routenzug",
        priority: "low",
        sort: "1"
      },
      {
        title: "Verpackung abholen",
        receiver: "Routenzug",
        priority: "info",
        sort: "99"
      }
    ],
    roles: ["sender"]
  },
  { title: "Routenzug", name: "routenzug", roles: ["receiver"] },
  { title: "Rennameise", name: "rennameise", roles: ["receiver"] },
  {
    title: "Wasserspender",
    name: "wasserspender",
    roles: ["sender"],
    commands: [
      {
        title: "Neue Wassergalonen",
        priority: "info",
        receiver: "Routenzug",
        sort: "99"
      }
    ]
  }
];

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/client/index.html");
});
app.get("/:file", (req, res) => {
  res.sendFile(__dirname + "/client/" + req.params.file);
});
app.get("/static/:file", (req, res) => {
  res.sendFile(__dirname + "/client/static/" + req.params.file);
});
app.get("/static/icons/:file", (req, res) => {
  res.sendFile(__dirname + "/client/static/icons/" + req.params.file);
});
app.get("/dist/:file", (req, res) => {
  res.sendFile(__dirname + "/client/dist/" + req.params.file);
});

server.listen(port, () => {
  console.log("Server startet on Port " + port + "...");
});

io.sockets.on("connection", socket => {
  connections.push(socket);
  console.log("Connected: %s sockets connected", connections.length);
  socket.emit("stations", stations);

  socket.on("station selected", station => {
    socket.station = station;
    database.tasks_for(socket.station, result => {
      socket.emit("tasks", result);
    });
    database.tasks_from(socket.station, result => {
      socket.emit("orders", result);
    });
  });

  socket.on("disconnect", data => {
    connections.splice(connections.indexOf(socket), 1);
    console.log("Disconneceted: %s sockets connected", connections.length);
  });

  socket.on("new task", data => {
    database.tasks_from(data.sender, result => {
      // Prüfe ob die Task bereits vorhanden ist
      let task = result.find(f => f.command.title == data.command.title);
      if (!task) {
        // neue Taks erstellen
        data.timestamp = Date.now();
        database.new_task(data, () => {
          database.tasks_for(data.command.receiver, result => {
            sendTasks(result, data.command.receiver);
          });
          database.tasks_from(data.sender, result => {
            sendOrders(result, data.sender);
          });
        });
      }
    });
  });

  socket.on("close task", data => {
    database.close_task(data.id, result => {
      database.tasks_for(socket.station, result => {
        sendTasks(result, socket.station);
      });
      database.tasks_from(data.sender, result => {
        sendOrders(result, data.sender);
      });
    });
  });

  socket.on("cancel task", data => {
    database.cancel_task(data.id, result => {
      database.tasks_for(data.receiver, result => {
        sendTasks(result, data.receiver);
      });
      database.tasks_from(socket.station, result => {
        sendOrders(result, socket.station);
      });
    });
  });

  function sendTasks(tasks, receiver) {
    connections.forEach(socket => {
      if (socket.station == receiver) {
        socket.emit("tasks", tasks);
      }
    });
  }

  function sendOrders(orders, sender) {
    connections.forEach(socket => {
      if (socket.station == sender) {
        socket.emit("orders", orders);
      }
    });
  }
});
